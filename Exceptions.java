import java.util.Scanner;


public class Exceptions {
	public static void main(String[] args) throws LenExceptions{
		System.out.println("Введите текст:");
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		char [] char_array = str.toCharArray();
		String [] str_array = str.split(" ");
		String text = "";
		for(int i = 0; i < char_array.length; i++) {
			if(char_array[i] == '"' || char_array[i] == ';' || char_array[i] == ':' || char_array[i] == '?'
			|| char_array[i] == ',' || char_array[i] == '.' || char_array[i] == '!' || char_array[i] == '@'
			|| char_array[i] == '-' || char_array[i] == '#' || char_array[i] == '№') {
				continue;
			}else {
				text += char_array[i];
			}
		}
		String [] text_array = text.split(" ");
		for(int i = 0; i < text_array.length; i++) {
			String s = "";
			s = str_array[i];
			try {
				if(text_array[i].length() > 10) {
					throw new LenExceptions("Ошибка. Очередное слово, содержащее более 10 букв.");
				}
			}catch(LenExceptions e) {
				s = e.getMessage();
			}finally {
				System.out.println(s);
			}
		}	
	}
}



class LenExceptions extends Exception{
	public LenExceptions(String message){    
        super(message);    
    }
}
